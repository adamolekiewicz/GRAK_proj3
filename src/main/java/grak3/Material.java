package grak3;

import javafx.scene.paint.Color;

/**
 * Material
 */
public class Material {

    public double specular, diffuse, ambient, gloss;
    public Color color;

    public Material(
        Color _color,
        double _specular,
        double _diffuse,
        double _ambient,
        double _gloss
    ) {
        specular = _specular;
        diffuse = _diffuse;
        ambient = _ambient;
        color = _color;
        gloss = _gloss;
    }
}