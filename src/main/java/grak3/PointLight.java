package grak3;

import org.ejml.simple.SimpleMatrix;

/**
 * PointLight
 */
public class PointLight {

    public SimpleMatrix position;
    public double intensity;

    public PointLight(
        double x,
        double y,
        double z,
        double _intensity
    ) {
        intensity = _intensity;
        position = new SimpleMatrix(new double[][]{
            {x},
            {y},
            {z},
            {1}
        });
    }
}