package grak3;

import org.ejml.simple.SimpleMatrix;

/**
 * Sphere
 */
public class Sphere {

    public double radius;
    public SimpleMatrix position, spherePoint, measurePoint;
    public Material material;

    public Sphere(
        double x,
        double y,
        double z,
        double _radius,
        Material _material
    ) {
        radius = _radius;
        material = _material;
        position = new SimpleMatrix(new double[][]{
            {x},
            {y},
            {z},
            {1}
        });
        spherePoint = new SimpleMatrix(new double[][]{
            {radius},
            {0},
            {0},
            {0}
        });
        measurePoint = new SimpleMatrix(new double[][]{
            {0},
            {1.0},
            {0},
            {0}
        }).plus(position);

    }

    public Sphere(Sphere old) {
        radius = old.radius;
        position = old.position;
        spherePoint = old.spherePoint;
        material = old.material;
    }
}