package grak3;

import static grak3.Transformations.dotProduct;
import static grak3.Transformations.getYRotationMatrix;
import static grak3.Transformations.normalize;
import static grak3.Transformations.transformCameraToProjection;
import static grak3.Transformations.transformProjectionToScreen;
import static grak3.Transformations.transformWorldToCamera;
import static grak3.Transformations.vectorLength;

import java.util.Arrays;
import java.util.Comparator;

import org.ejml.simple.SimpleMatrix;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.geometry.VPos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.Color;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;

/**
 * Main
 */
public class Main extends Application {

    public static void main(String[] args) {
        launch();
    }

    private GraphicsContext context;
    private final double WINDOW_WIDTH = 800.0, WINDOW_HEIGHT = 600.0;
    @Override
    public void start(Stage primaryStage) throws Exception {
        Group root = new Group();
        Canvas canvas = new Canvas(WINDOW_WIDTH, WINDOW_HEIGHT);
        Scene scene = new Scene(root, WINDOW_WIDTH, WINDOW_HEIGHT);
        context = canvas.getGraphicsContext2D();

        context.setFill(Color.BLACK);
        context.setStroke(Color.WHITE);
        context.fillRect(0, 0, 800, 600);
        context.setTextAlign(TextAlignment.LEFT);
        context.setTextBaseline(VPos.TOP);

        scene.addEventHandler(KeyEvent.KEY_PRESSED, keyPressHandler);
        scene.addEventHandler(KeyEvent.KEY_RELEASED, keyReleaseHandler);
        root.getChildren().add(canvas);

        primaryStage.setScene(scene);

        primaryStage.show();

        setupScene();
        AnimationTimer timer = new AnimationTimer(){
            @Override
            public void handle(long now) {
                updateScene();
                drawScene();
            }
        };
        timer.start();
    }
    
    private Camera camera;
    private Sphere[] spheres;
    private PointLight light;
    private SimpleMatrix[] posCamera, posProjection, posScreen,
        radCamera, radProjection, radScreen;
    private Integer[] sphereIs;

    private void setupScene() {
        camera = new Camera();
        spheres = new Sphere[]{
            new Sphere(1.0, 0.0, 2.5, 0.5, new Material(
                Color.CORAL, 2.0, 0.0, 0.0, 32)),
            new Sphere(-1.0, 0.0, 2.5, 0.5, new Material(
                Color.BLUE, 0.0, 1.0, 0.0, 0)),
            new Sphere(1.0, 0.0, 4.5, 0.5, new Material(
                Color.DARKORANGE, 0.3, 0.7, 0.03, 4)),
            new Sphere(-1.0, 0.0, 4.5, 0.5, new Material(
                Color.BEIGE, 0.7, 0.1, 0.03, 8))
        };
        light = new PointLight(0, 4, 0, 1);

        posCamera = new SimpleMatrix[spheres.length];
        posProjection = new SimpleMatrix[spheres.length];
        posScreen = new SimpleMatrix[spheres.length];
        radCamera = new SimpleMatrix[spheres.length];
        radProjection = new SimpleMatrix[spheres.length];
        radScreen = new SimpleMatrix[spheres.length];

        sphereIs = new Integer[]{0, 1, 2, 3};
    }

    private void updateScene() {
        camera.rotation = camera.rotation.plus(camera.rotationSpeed);
        camera.position = camera.position.plus(
            getYRotationMatrix(camera.rotation).mult(camera.speed));
        double newFov = camera.fov + camera.zoomSpeed;
        camera.fov = newFov > 120 ? 120 : newFov < 30 ? 30 : newFov;
    }

    private void drawScene() {
        context.setFill(Color.BLACK);
        context.fillRect(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);
        
        SimpleMatrix lightPositionCamera = transformWorldToCamera(
            light.position, camera.position, camera.rotation);
        
        for(int i = 0; i < spheres.length; i++) {
            posCamera[i] = transformWorldToCamera(
                spheres[i].position, camera.position, camera.rotation);
        }
        Arrays.sort(sphereIs, Comparator.comparingDouble(
            (Integer i) -> posCamera[i].get(2)));
        
        for(int si = 0; si < sphereIs.length; si++) {
            int i = sphereIs[si];
            radCamera[i] = spheres[i].spherePoint.plus(posCamera[i]);
            posProjection[i] = transformCameraToProjection(
                posCamera[i], camera);
            radProjection[i] = transformCameraToProjection(
                radCamera[i], camera);
            posScreen[i] = transformProjectionToScreen(
                posProjection[i], WINDOW_WIDTH, WINDOW_HEIGHT);
            radScreen[i] = transformProjectionToScreen(
                radProjection[i], WINDOW_WIDTH, WINDOW_HEIGHT);
            
            double screenRadius = vectorLength(radScreen[i].minus(posScreen[i]));
            
            int fragXStart = (int)(Math.round(posScreen[i].get(0) - screenRadius));
            int fragXEnd = (int)(Math.round(posScreen[i].get(0) + screenRadius));
            int fragYStart = (int)(Math.round(posScreen[i].get(1) - screenRadius));
            int fragYEnd = (int)(Math.round(posScreen[i].get(1) + screenRadius));

            double camXStart = posCamera[i].get(0) - spheres[i].radius;
            double camYEnd = posCamera[i].get(1) + spheres[i].radius;
            double camStep = spheres[i].radius * 2.0 / (fragXEnd - fragXStart);

            double cx = camXStart;
            for(
                int sx = Math.max(0, fragXStart);
                sx <= Math.min(WINDOW_WIDTH, fragXEnd);
                sx++
            ){
                double cy = camYEnd;
                for(
                    int sy = Math.max(0, fragYStart);
                    sy <= Math.min(WINDOW_HEIGHT, fragYEnd);
                    sy++
                ) {
                    // Per pixel operation
                    double sk = Math.pow(screenRadius, 2.0) -
                        Math.pow(sx - posScreen[i].get(0), 2.0) -
                        Math.pow(sy - posScreen[i].get(1), 2.0);
                    if(sk >= 0 && posScreen[i].get(2) >= 0) {
                        double ck = Math.pow(spheres[i].radius, 2.0) -
                            Math.pow(cx - posCamera[i].get(0), 2.0) -
                            Math.pow(cy - posCamera[i].get(1), 2.0);
                        SimpleMatrix p = new SimpleMatrix(new double[][]{
                            {cx},
                            {cy},
                            {Math.sqrt(ck) + posCamera[i].get(2)},
                            {1.0}
                        });
                        SimpleMatrix normal = normalize(p.minus(posCamera[i]));
                        SimpleMatrix lightDirection =
                            normalize(lightPositionCamera.minus(p));
                        SimpleMatrix viewDirection =
                            normalize(p.negative());
                        SimpleMatrix incidence = lightDirection.negative();
                        SimpleMatrix reflection = incidence.minus(
                            normal.scale(dotProduct(normal, incidence) * 2.0));
                        
                        double ambient = spheres[i].material.ambient;
                        double diffuse = spheres[i].material.diffuse * Math.max(
                            0.0,
                            dotProduct(
                                normalize(lightDirection),
                                normalize(normal)));
                        double specular = spheres[i].material.specular *
                            Math.pow(Math.max(0.0,
                                dotProduct(viewDirection, reflection)),
                                spheres[i].material.gloss);
                        Color outColor = new Color(
                            Math.max(0.0, Math.min(1.0, spheres[i].material.color
                                .getRed() * (ambient + diffuse + specular))),
                            Math.max(0.0, Math.min(1.0, spheres[i].material.color
                                .getGreen() * (ambient + diffuse + specular))),
                            Math.max(0.0, Math.min(1.0, spheres[i].material.color
                                .getBlue() * (ambient + diffuse + specular))),
                            spheres[i].material.color.getOpacity());
                        context.getPixelWriter().setColor(
                            sx, sy, outColor);
                    }
                    cy -= camStep;
                }
                cx += camStep;
            }
        }
    }

    private final double CAM_SPEED = 0.05, CAM_ROT_SPEED = 0.02,
        CAM_ZOOM_SPEED = 1; 
    private final EventHandler<KeyEvent> keyPressHandler = new EventHandler<>() {
        @Override
        public void handle(KeyEvent event) {
            switch (event.getCode()) {
            case W:
                camera.speed.set(2, -CAM_SPEED);
                // Inverted due to Z axis flip during projection
                break;
            case S:
                camera.speed.set(2, CAM_SPEED);
                break;
            case A:
                camera.speed.set(0, -CAM_SPEED);
                break;
            case D:
                camera.speed.set(0, CAM_SPEED);
                break;
            case Q:
                camera.speed.set(1, CAM_SPEED);
                break;
            case E:
                camera.speed.set(1, -CAM_SPEED);
                break;
            case UP:
                camera.rotationSpeed.set(0, -CAM_ROT_SPEED);
                // Inverted due to author's preference
                break;
            case DOWN:
                camera.rotationSpeed.set(0, CAM_ROT_SPEED);
                break;
            case LEFT:
                camera.rotationSpeed.set(1, CAM_ROT_SPEED);
                break;
            case RIGHT:
                camera.rotationSpeed.set(1, -CAM_ROT_SPEED);
                break;
            case PAGE_UP:
                camera.zoomSpeed = CAM_ZOOM_SPEED;
                break;
            case PAGE_DOWN:
                camera.zoomSpeed = -CAM_ZOOM_SPEED;
                break;
            default:
                break;
            }
        }
    };

    private EventHandler<KeyEvent> keyReleaseHandler = new EventHandler<>() {
        @Override
        public void handle(KeyEvent event) {
            switch (event.getCode()) {
            case W:
                // camera.setVForward(0);
                camera.speed.set(2, 0);
                break;
            case S:
                // camera.setVForward(0);
                camera.speed.set(2, 0);
                break;
            case A:
                // camera.setVRight(0);
                camera.speed.set(0, 0);
                break;
            case D:
                // camera.setVRight(0);
                camera.speed.set(0, 0);
                break;
            case Q:
                // camera.setVUp(0);
                camera.speed.set(1, 0);
                break;
            case E:
                // camera.setVUp(0);
                camera.speed.set(1, 0);
                break;
            case UP:
                // camera.setVRotX(0);
                camera.rotationSpeed.set(0, 0);
                break;
            case DOWN:
                // camera.setVRotX(0);
                camera.rotationSpeed.set(0, 0);
                break;
            case LEFT:
                // camera.setVRotY(0);
                camera.rotationSpeed.set(1, 0);
                break;
            case RIGHT:
                // camera.setVRotY(0);
                camera.rotationSpeed.set(1, 0);
                break;
            case PAGE_UP:
                camera.zoomSpeed = 0;
                break;
            case PAGE_DOWN:
                camera.zoomSpeed = 0;
                break;
            default:
                break;
            }
        }
    };

}