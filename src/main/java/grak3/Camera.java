package grak3;

import org.ejml.simple.SimpleMatrix;

public class Camera {
    public SimpleMatrix position, rotation, speed, rotationSpeed;
    public double zoomSpeed, fov;

    public Camera() {
        position = new SimpleMatrix(new double[][]{
            {0.0, 0.0, 0.0, 1.0}
        }).transpose();
        rotation = new SimpleMatrix(new double[][]{
            {0.0, Math.PI, 0.0, 1.0}
        }).transpose();
        speed = new SimpleMatrix(new double[][]{
            {0.0, 0.0, 0.0, 0.0}
        }).transpose();
        rotationSpeed = new SimpleMatrix(new double[][]{
            {0.0, 0.0, 0.0, 0.0}
        }).transpose();
        fov = 90;
    }
}
